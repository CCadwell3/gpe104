﻿using UnityEngine;
using System.Collections;

public class CircTrigger : MonoBehaviour
{

    public ZombieAi zombieAi;

    //within hearing range
    void OnTriggerEnter2D(Collider2D o)
    {

        if (o.gameObject.tag == "Player")
        {
            zombieAi.inViewCone = true;
        }
    }
    //exit hearing range
    void OnTriggerExit2D(Collider2D o)
    {


        if (o.gameObject.tag == "Player")
        {
            zombieAi.inViewCone = false;
        }
    }
}