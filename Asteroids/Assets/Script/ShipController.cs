﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour
{

    public float rotationSpeed = 100.0f;//editable rotation value
    public float thrustForce = 3f;//editable speed value

    public AudioClip crash;
    public AudioClip shoot;

    public GameObject bullet;
    private GameController gameController;

    void Start()
    {
        // Get a reference to the game controller object and the script
        GameObject gameControllerObject =
            GameObject.FindWithTag("GameController");

        gameController =
            gameControllerObject.GetComponent<GameController>();
       
    }

    void FixedUpdate()
    {

        // Rotate the ship by axes
        transform.Rotate(0, 0, -Input.GetAxis("Horizontal") *
            rotationSpeed * Time.deltaTime);

        // Thrust the ship by axes
        GetComponent<Rigidbody2D>().
            AddForce(transform.up * thrustForce *
                Input.GetAxis("Vertical"));

        // fire
        if (Input.GetKeyDown("space"))
            ShootBullet();

    }

    void OnTriggerEnter2D(Collider2D c)
    {

        // boom if not bullet
        if (c.gameObject.tag != "Bullet")
        {

            AudioSource.PlayClipAtPoint
                (crash, Camera.main.transform.position);//play explosion

            // Move the ship to the centre of the screen
            transform.position = new Vector3(0, 0, 0);

            // Remove all velocity from the ship
            GetComponent<Rigidbody2D>().
                velocity = new Vector3(0, 0, 0);

            gameController.DecrementLives();
        }
    }

    void ShootBullet()
    {

        // Spawn a bullet
        Instantiate(bullet,
            new Vector3(transform.position.x, transform.position.y, 0),
            transform.rotation);

        // Play a shoot sound
        AudioSource.PlayClipAtPoint(shoot, Camera.main.transform.position);
    }
    //offscreen death
    void OnBecameInvisible()
    {
        AudioSource.PlayClipAtPoint
                (crash, Camera.main.transform.position);
        gameController.DecrementLives();
        transform.position = new Vector3(0, 0, 0);
        GetComponent<Rigidbody2D>().
                velocity = new Vector3(0, 0, 0);
    }
}