﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour
{

    public float bulletSpeed = 400.0f;//editable speed
    public float bulletLife = 1f;//editable bullet time to live
    // Use this for initialization
    void Start()
    {
        // Set the bullet to destroy itself after life exceeded
        Destroy(gameObject, bulletLife);

        // Push the bullet in the direction it is facing
        GetComponent<Rigidbody2D>()
            .AddForce(transform.up * bulletSpeed);
    }

}