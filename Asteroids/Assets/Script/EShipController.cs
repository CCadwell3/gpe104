﻿using UnityEngine;
using System.Collections;

public class EShipController : MonoBehaviour
{

    public AudioClip destroy; //audiofile for destruction sound

    public float shipSpeed = 50.0f;//editable speed value

    private GameController gameController; 

    // Use this for initialization
    void Start()
    {

        // Get a reference to the game controller object and the script
        GameObject gameControllerObject =
            GameObject.FindWithTag("GameController");

        gameController =
            gameControllerObject.GetComponent<GameController>();

        // Push the asteroid in the direction it is facing at asteroidSpeed
        GetComponent<Rigidbody2D>()
            .AddForce(transform.up * shipSpeed);

        // Give a random angular velocity/rotation
        GetComponent<Rigidbody2D>()
            .angularVelocity = Random.Range(-0.0f, 90.0f);

    }

    void OnCollisionEnter2D(Collision2D c)
    {
        //check for collision with bullet
        if (c.gameObject.tag.Equals("Bullet"))
        {

            // Destroy the bullet
            Destroy(c.gameObject);

            // Play a sound
            AudioSource.PlayClipAtPoint(
                destroy, Camera.main.transform.position);

            // Add to the score
            gameController.IncrementScore();

            // Destroy the current asteroid
            Destroy(gameObject);

        }

    }
    //destroy offscreen, no score
    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}