﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController instance = null;
    public GameObject[] eArray;//Editable holder for enemies
    public GameObject asteroid;//holder for asteroid object
    public GameObject Ship;//holder for ship object
    
    private int score;
    private int hiscore;
    public int maxLives = 3;
    private int lives;
    public int increaseEachWave = 4;

    public Text scoreText;
    public Text livesText;
    public Text hiscoreText;

    public float MinX = -10;//editable spawn position values for enemies
    public float MaxX = 10;
    public float MinY = -5;
    public float MaxY = 5;

    //Singleton  only one instance
    void Awake()
    {
        if (GameController.instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject); 
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
   
        hiscore = PlayerPrefs.GetInt("hiscore", 0);
        BeginGame();
    }

    // Update is called once per frame
    void Update()
    {

        // Quit if player presses escape
        if (Input.GetKey("escape"))
            Application.Quit();
        while ((GameObject.FindGameObjectsWithTag("Enemy").Length + GameObject.FindGameObjectsWithTag("Asteroid").Length) <= 2)//limit spawns
        {

            // Spawn an asteroid

            int eNumber = Random.Range(0, 6);//set random array number for spawn selection
            float x = Random.Range(MinX, MaxX);//set custom range for random position
            float y = Random.Range(MinY, MaxY);//set custom range for random position

            Instantiate(eArray[eNumber],
                new Vector3(x, y, 0),//use random ranges for x and y placement
                Quaternion.Euler(0, 0, Random.Range(-0.0f, 359.0f))); //random angle because I cant get LookAt to work
        }
    }

    void BeginGame()
    {
        lives = maxLives;
        score = 0;

        // Prepare the HUD
        scoreText.text = "SCORE:" + score;
        hiscoreText.text = "HISCORE: " + hiscore;
        livesText.text = "LIVES: " + lives;

        SpawnAsteroids();
    }

    void SpawnAsteroids()
    {
        DestroyExistingAsteroids();
        
    }
    public void IncrementScore()
    {
        score++;

        scoreText.text = "SCORE:" + score;

        if (score > hiscore)
        {
            hiscore = score;
            hiscoreText.text = "HISCORE: " + hiscore;

            // Save the new hiscore
            PlayerPrefs.SetInt("hiscore", hiscore);
        }
    }

    public void DecrementLives()
    {
        lives--;
        livesText.text = "LIVES: " + lives;

        // Has player run out of lives?
        if (lives < 1)
        {
            // Restart the game
            BeginGame();
        }
        else if (lives > 0)
        {
            SpawnAsteroids();
        }
    }

    void DestroyExistingAsteroids()
    {
        GameObject[] asteroids =
            GameObject.FindGameObjectsWithTag("Asteroid");

        foreach (GameObject current in asteroids)
        {
            GameObject.Destroy(current);
        }

    }
    

}