﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneSelector : MonoBehaviour
{
 
    public void ToTitle()
    {
        SceneManager.LoadScene("Title");
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Level");
    }
}