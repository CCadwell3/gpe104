﻿using UnityEngine;
using System.Collections;

public class SaveLoad : MonoBehaviour
{

    void load()
    {
       transform.position = new Vector3(PlayerPrefs.GetFloat("x"), PlayerPrefs.GetFloat("y"));
    }

    void save()
    {
        PlayerPrefs.SetFloat("x", transform.position.x);
        PlayerPrefs.SetFloat("y", transform.position.y);
    }
}