﻿using System;
using UnityEngine;

public class Movement : MonoBehaviour
{
    Vector2 pos1;  //container for starting position
    float canMove = 1;  //value for movement disabled
    public float moveSpeed = 10f; //Speed modifier
    //initialize
    void Start()
    {
        pos1 = gameObject.transform.position;  //set start position
    }
    //keep looking
    void Update()
    {
        if (Input.GetKey(KeyCode.P))//check for P and change move var
        {
            canMove = 0;
        }

        if (Input.GetKeyUp(KeyCode.P)) //check for P and change move var
        {
            canMove = 1;
        }

        if (Input.GetKeyDown(KeyCode.Q)) //  get q button, disable sprite
        {
            gameObject.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Space))  //button press spacebar
        {
            gameObject.transform.position = pos1;  //reset to start
        }

        if (Input.GetKey("escape"))   // Close application on "escape"
        {
            Application.Quit();
        }
        if (canMove == 1)
        {
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))//Block for movement while holding shift
            {
                if (Input.GetKeyDown(KeyCode.UpArrow))
                    transform.Translate(Vector3.up);//move up 1

                if (Input.GetKeyDown(KeyCode.DownArrow))
                    transform.Translate(Vector3.down);//move down 1

                if (Input.GetKeyDown(KeyCode.LeftArrow))
                    transform.Translate(Vector3.left);//move left 1

                if (Input.GetKeyDown(KeyCode.RightArrow))
                    transform.Translate(Vector3.right);//move right 1
            }
            if (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.LeftShift))//block for movement while NOT holding shift
            {
                if (Input.GetKey(KeyCode.RightArrow))
                    transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);//move right at a speed of 1/s

                if (Input.GetKey(KeyCode.LeftArrow))
                    transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);//move left at a speed of 1/s

                if (Input.GetKey(KeyCode.UpArrow))
                    transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);//move up at a speed of 1/s

                if (Input.GetKey(KeyCode.DownArrow))
                    transform.Translate(Vector3.down * moveSpeed * Time.deltaTime);//move down at a speed of 1/s
            }
        }
        if (canMove == 0)
        {
            if (Input.GetKey(KeyCode.UpArrow))
                transform.Translate(0, 0, 0);

            if (Input.GetKey(KeyCode.DownArrow))
                transform.Translate(0, 0, 0);

            if (Input.GetKey(KeyCode.LeftArrow))
                transform.Translate(0, 0, 0);

            if (Input.GetKey(KeyCode.RightArrow))
                transform.Translate(0, 0, 0);
        }
    }
}